import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivateEmailComponent } from './activate-email/activate-email.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterCondiddateComponent } from './register-condiddate/register-condiddate.component';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';
import { RegisterLayoutComponent } from './register-layout/register-layout.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterLayoutComponent,
  children: [
    { path: 'condidate', component: RegisterCondiddateComponent },
    { path: 'employee', component: RegisterEmployeeComponent }
    ] 
  }, 
  { path: 'activate_email' , component: ActivateEmailComponent},
  { path: 'forget_password' , component: ForgetPasswordComponent},
  { path: 'change_password' , component: ChangePasswordComponent},
  { path: 'reset_password' , component: ResetPasswordComponent},
  {
    path: '', 
    redirectTo: 'login',
    pathMatch: 'prefix'
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
