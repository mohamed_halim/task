import { Component, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailValidator, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MainService } from 'src/app/services/main.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  constructor(
    public sharedservice: MainService,
    private _snackBar: MatSnackBar,
    public router:Router

  ) { }

  form: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required , Validators.email]),
    password: new FormControl('', Validators.required),
  });

  ngOnInit(){
    if (localStorage.getItem('UserInfo') !== null){
      this.router.navigate(['./Home'])
    }
  }

  submit() {
    if (this.form.valid) {
      this.sharedservice.showspinner();

      this.sharedservice.Login(this.form.value).subscribe( res => {
        if(res['status'] == 'Fail'){
          this.sharedservice.hidespinner()
          this._snackBar.open(res['errorMessage'], '', {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 6 * 1000,
          });

          if(res['errorMessage'] !== 'Invalid email or password.'){
            this.ActiveEmail()
          }
        }  else {
          this.sharedservice.hidespinner()
          localStorage.setItem('UserInfo', JSON.stringify(res['serverParams']))
          localStorage.setItem('token', res['serverParams']['AccessToken'])
          this.router.navigate(['./Home'])    
        }
      },
      error =>{
        this.sharedservice.hidespinner()
        this._snackBar.open(error['error']['title'], '', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 5 * 1000,
        });
      }
      )
    }
  }

  ActiveEmail(){
    this.sharedservice.ResendActivationEmail(this.form.value).subscribe( res => {
      if(res['status'] == 'Fail'){
        this.sharedservice.hidespinner()
        this._snackBar.open(res['errorMessage'], '', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 5 * 1000,
        });
      }  else {
        this.sharedservice.hidespinner()
        this._snackBar.open(res['serverParams']['Message'], '', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 6 * 1000,
        });
        this.router.navigate(['./activate_email'])
        
      }
    },
    error =>{
      this.sharedservice.hidespinner()
      this._snackBar.open(error['error']['title'], '', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
        duration: 5 * 1000,
      });
    }

    )
  }
}
