import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators , NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.sass'],
  providers: [MainService]
})
export class RegisterEmployeeComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup : FormGroup;

  
  constructor(
    public _formBuilder: FormBuilder,
    public sharedservice: MainService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required ,Validators.minLength(6)]],
    } , { validator: this.checkPasswords });

    this.secondFormGroup = this._formBuilder.group({
      jobsNumber: ['', Validators.required],
      hiringNeed: ['', Validators.required],
      currentTitle: ['', Validators.required],
      phoneNumber: ['', Validators.required],
    });

    this.thirdFormGroup = this._formBuilder.group({
      zipCode: ['', Validators.required],
      trackingSystem: ['', Validators.required],
      marketingOption: ['', Validators.required],
      specifyMarketing: ['', Validators.required],
    });
  }

  checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;
  
    return pass === confirmPass ? null : { notSame: true };
  }

  chk_pass(){
    if(this.firstFormGroup.controls.password.value !== this.firstFormGroup.controls.confirmPassword.value )
    this._snackBar.open('The password and confirmation password do not match', '', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 5 * 1000,
    });
    
  }
  
  save(){
 
  let form_value = {
    ...this.firstFormGroup.value,
    ...this.secondFormGroup.value,
    ...this.thirdFormGroup.value
  }

  this.sharedservice.showspinner()
  this.sharedservice.EmployerRegister(form_value).subscribe( res => {
    if(res['status'] == 'Fail'){
      this.sharedservice.hidespinner()
      this._snackBar.open(res['errorMessage'], '', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
        duration: 5 * 1000,
      });
    }  else {
      this.sharedservice.hidespinner()
      this._snackBar.open(res['serverParams']['Message'], '', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
        duration: 5 * 1000,
      });
    }
  },
  error =>{
    this.sharedservice.hidespinner()
    this._snackBar.open(error['error']['title'], '', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 5 * 1000,
    });
  }
  )

  }

}
