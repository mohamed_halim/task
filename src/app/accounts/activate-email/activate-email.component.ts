import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-activate-email',
  templateUrl: './activate-email.component.html',
  styleUrls: ['./activate-email.component.sass']
})
export class ActivateEmailComponent implements OnInit {

  constructor(
    public sharedservice: MainService,
    private _snackBar: MatSnackBar
    ) { }
  
    form: FormGroup = new FormGroup({
      email: new FormControl('' , [Validators.required, Validators.email]),
      token: new FormControl('', Validators.required),
     });
  
    ngOnInit(){}
  
    submit() {
      if (this.form.valid) {
        this.sharedservice.showspinner()
        this.sharedservice.ConfirmMail(this.form.value).subscribe( res => {
          if(res['status'] == 'Fail'){
            this.sharedservice.hidespinner()
            this._snackBar.open(res['errorMessage'], '', {
              horizontalPosition: 'center',
              verticalPosition: 'top',
              duration: 5 * 1000,
            });
          }  else {
            this.sharedservice.hidespinner()
            this._snackBar.open(res['serverParams']['Message'], '', {
              horizontalPosition: 'center',
              verticalPosition: 'top',
              duration: 5 * 1000,
            });
          }
        },
        error =>{
          this.sharedservice.hidespinner()
          this._snackBar.open(error['error']['title'], '', {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 5 * 1000,
          });
        },
        )
      }
    }
}
