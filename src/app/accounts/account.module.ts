import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterEmployeeComponent } from '../accounts/register-employee/register-employee.component';

import { AccountRoutingModule } from './account-routing.module';
import { RegisterCondiddateComponent } from './register-condiddate/register-condiddate.component';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { RegisterLayoutComponent } from './register-layout/register-layout.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ActivateEmailComponent } from './activate-email/activate-email.component';

@NgModule({
  declarations: [
    RegisterEmployeeComponent,
    RegisterCondiddateComponent,
    LoginComponent,
    ForgetPasswordComponent,
    ChangePasswordComponent,
    RegisterLayoutComponent,
    ResetPasswordComponent,
    ActivateEmailComponent
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    MatStepperModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule, 
    FormsModule, 
    ReactiveFormsModule,
    MatSnackBarModule,
    MatCardModule,
    MatCheckboxModule,
    NgxMatFileInputModule,
    MatTabsModule
  ]
})
export class AccountModule { }
