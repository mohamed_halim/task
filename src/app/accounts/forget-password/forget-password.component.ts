import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.sass']
})
export class ForgetPasswordComponent implements OnInit {

  constructor(
  public sharedservice: MainService,
  private _snackBar: MatSnackBar,
  public router:Router
  ) { }

  form: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required , Validators.email]),
   });

  ngOnInit(){}

  submit() {
    if (this.form.valid) {
      this.sharedservice.showspinner()
      this.sharedservice.ForgetPassword({...this.form.value, "platformType": 0}).subscribe( res => {
        if(res['status'] == 'Fail'){
          this.sharedservice.hidespinner()
          this._snackBar.open(res['errorMessage'], '', {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 5 * 1000,
          });
        }  else {
          this.sharedservice.hidespinner();
          this._snackBar.open(res['serverParams']['Message'], '', {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 5 * 1000,
          });
          this.router.navigate(['./reset_password'])
        }
      },
      error =>{
        this.sharedservice.hidespinner()
        this._snackBar.open(error['error']['title'], '', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 5 * 1000,
        });
      }
      )
    }
  }

}
