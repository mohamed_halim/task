import { HttpErrorResponse } from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MainService } from 'src/app/services/main.service';


@Component({
  selector: 'app-register-condiddate',
  templateUrl: './register-condiddate.component.html',
  styleUrls: ['./register-condiddate.component.sass'],
  providers: [MainService]
})
export class RegisterCondiddateComponent implements OnInit {

  file: any;
  isLinear = false;
  resumeName :any;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup : FormGroup;
  fourFormGroup : FormGroup;
  
  constructor(
    public _formBuilder: FormBuilder,
    public sharedservice: MainService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      jobTitle: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      fullName: ['', Validators.required],
      email:['', [Validators.required, Validators.email]],
      address: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      resume: ['', Validators.required],
    });

    this.secondFormGroup = this._formBuilder.group({
      companyName: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      title: ['', Validators.required],
      dateFrom: ['', Validators.required],
      dateTo: ['', Validators.required],
      stillWorkThere: [true, Validators.required],
      description: ['', Validators.required]
    });

    this.thirdFormGroup = this._formBuilder.group({
      school: ['', Validators.required],
      dateFrom: ['', Validators.required],
      dateTo: ['', Validators.required],
      degree: ['', Validators.required],
      areaOfStudy: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.fourFormGroup = this._formBuilder.group({
      willToWorkRemotely: [true, Validators.required],
      countryId: [null, Validators.required],
      minimumDesirePay: [null, Validators.required],
      isLookingActive: [true, Validators.required],
      isProfileSearchable: [true, Validators.required],
    });



    this.firstFormGroup.controls.resume.valueChanges.subscribe((files: any) => {
      if (!Array.isArray(files)) {
        this.file = [files];
        this.resumeName = this.file[0].name
      } else {
        this.file = files;
        this.resumeName = this.file[0].name
      }
    })
  }


  save(){
 
  let form_value = {
    ...this.firstFormGroup.value,
    resume:this.resumeName,
    experiences:[
      this.secondFormGroup.value,
    ],
    educations:[
      this.thirdFormGroup.value
    ],
    ...this.fourFormGroup.value
  }
  this.sharedservice.showspinner()
  this.sharedservice.CandidateRegister(form_value).subscribe( res => {
    if(res['status'] == 'Fail'){
      this.sharedservice.hidespinner()
      this._snackBar.open(res['errorMessage'], '', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
        duration: 5 * 1000,
      });
    }  else {
      this.sharedservice.hidespinner()
      this._snackBar.open(res['serverParams']['Message'], '', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
        duration: 5 * 1000,
      });
    }
   
  },
  error =>{
    this.sharedservice.hidespinner()
    this._snackBar.open(error['error']['title'], '', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 5 * 1000,
    });
  }
  )

  }

}
