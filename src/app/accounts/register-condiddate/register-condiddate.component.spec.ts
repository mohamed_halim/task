import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCondiddateComponent } from './register-condiddate.component';

describe('RegisterCondiddateComponent', () => {
  let component: RegisterCondiddateComponent;
  let fixture: ComponentFixture<RegisterCondiddateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCondiddateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCondiddateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
