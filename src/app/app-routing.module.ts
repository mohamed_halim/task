import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';

const routes: Routes = [
  { path: 'Home' , component: HomePageComponent},
  {
    path: '', loadChildren : () => import('./accounts/account.module').then(x => x.AccountModule)
  },{ 
    path: '**', redirectTo: ''
   },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
