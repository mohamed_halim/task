import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(
    private router: Router,
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ) { }


  Login(user: object){
    return this.http.post<any>(`${environment.apiUrl}/Account/Login`, user);
  }


  EmployerRegister(user: object) {
    return this.http.post<any>(`${environment.apiUrl}/Users/EmployerRegister`, user);
  }

  CandidateRegister(user: object){
    return this.http.post<any>(`${environment.apiUrl}/Users/CandidateRegister`, user);

  }

  ForgetPassword(email:object){
    return this.http.post<any>(`${environment.apiUrl}/Account/ForgetPassword`, email);
  }

  ChangePassword(body:object){
    return this.http.post<any>(`${environment.apiUrl}/Account/ChangePassword`, body);
  }

  ResendActivationEmail(email:object){
    return this.http.post<any>(`${environment.apiUrl}/Account/ResendActivationEmail`, email);
  }

  ConfirmMail(body: object){
    return this.http.post<any>(`${environment.apiUrl}/Users/ConfirmMail`, body);
  }

  ResetPassword(body: object){
    return this.http.post<any>(`${environment.apiUrl}/Account/ResetPassword`, body);
  }

  showspinner(){
    this.spinner.show();
  }

  hidespinner(){
    this.spinner.hide();
  }
}
