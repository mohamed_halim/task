import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.sass']
})
export class HomePageComponent implements OnInit {

  UserInfo:any
  constructor(
    public router:Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('UserInfo') == null){
      this.router.navigate(['/login'])
    }
    this.UserInfo = localStorage.getItem('UserInfo')
    this.UserInfo = JSON.parse(this.UserInfo)
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['/login'])
  }

}
