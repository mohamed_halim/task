import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { filter, map } from "rxjs/operators";
import { Router } from '@angular/router';

@Injectable() 

export class AppInterceptor implements HttpInterceptor{

  constructor(
    public Router:Router
  ) {
  } 
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    if (token) {
      request = request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
    }

    return next.handle(request).pipe(
        // filter(event => event instanceof HttpResponse),
        // map(
        //   (event: HttpResponse<any>) => {
        //     if (event.body.message === 'Not Authrized User or Session Expiered') {
        //       // only parse xml response, pass all other responses to other interceptors
        //       localStorage.clear();
        //       this.Router.navigate(["./login"])
        //     }  
        //       return event;
            
           
        //   })

   );
  }


  
}